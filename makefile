#//CROSS_COMPILE = arm-angstrom-linux-gnueabi-
NAME          = dspi
DEBUG         = 1
CC           := $(CROSS_COMPILE)gcc --std=c99
CFLAGS       := -Wall -c -fPIC 
LDFLAGS      := -Wall -ldl -pthread
LIBS          = 
LIBS_PATH     = -L"/usr/lib" -L"/lib" 
INCLUDES      = 
INCL_PATH     = #-I"api_c"
ifeq ($(DEBUG), 1)
CC           := $(CC) -g -O0
CFLAGS       := $(CFLAGS) -DDEBUG
endif

TOPDIR        = .
SRCDIR        = $(TOPDIR)/src
SRCREL        = $(shell find $(SRCDIR) -type f -name *.c)
SOURCE        = $(wildcard $(SRCREL))
SRC           = $(notdir $(SOURCE))
OBJ           = $(SRC:.c=.o)
OBJECT        = $(addprefix $(OBJDIR)/, $(OBJ))
ifeq ($(DEBUG),1)
ELFDIR        = $(TOPDIR)/debug
ELFEXE        = $(ELFDIR)/main
ELFDLL        = $(ELFDIR)/$(NAME).so
else
ELFDIR        = $(TOPDIR)/release
ELFEXE        = $(ELFDIR)/main
ELFDLL        = $(ELFDIR)/$(NAME).so
endif
OBJDIR        = $(ELFDIR)/obj

include version.mk
BUILD_NEW    := $(shell expr $(BUILD) + 1)


all: $(OBJ) elf
	@echo $(ELFEXE) version $(VERSION) build $(BUILD)
	$(shell echo "VERSION = $(VERSION)" > version.mk)
	$(shell echo "BUILD   = $(BUILD_NEW)" >> version.mk)

version:
	@$(ELFEXE) -v

debug:
	gdb $(ELFEXE)

tui:
	gdb $(ELFEXE)

edit:
	vim $(SRCREL)

run:
	$(ELFEXE)

elf:
	@if [ ! -d "$(ELFDIR)" ]; then mkdir -p "$(ELFDIR)"; fi
	$(CC) -o $(ELFEXE) $(OBJECT) $(LDFLAGS) $(LIBS_PATH) $(LIBS) -W
	# $(CC) -shared -o $(ELFDLL) $(OBJECT) $(LDFLAGS) $(LIBS_PATH) $(LIBS) -W

VPATH         = $(shell find $(SRCDIR) -type d -printf %p:)

%.o:%.c
	@if ! [ -d $(OBJDIR) ]; then mkdir -p $(OBJDIR); fi
	$(CC) -o $(OBJDIR)/$@ $< $(CFLAGS) $(INCL_PATH) $(INCLUDES) -W -DVER="\"ver. $(VERSION) build $(BUILD)\""

test:
	@echo " 0 VPATH    =$(VPATH)"
	@echo " 1 SHELL    =$(SHELL)"
	@echo " 2 VERSION  =$(VERSION).$(BUILD) $(VER)"
	@echo " 3 EXEC=$(ELFEXE) --version $(VERSION) --build $(BUILD)"
	$(shell echo "VERSION = $(VERSION)" > version.mk)
	$(shell echo "BUILD   = $(BUILD_NEW)" >> version.mk)
	@echo " 4 LIBS_W   =$(wildcard LIBS)"
	@echo " 5 LIBS     =$(LIBS) "
	@echo " 6 LIBS_PATH=$(LIBS_PATH) "
	@echo " 7 INCLUDES =$(INCLUDES) "
	@echo " 8 INCL_PATH=$(INCL_PATH) "
	@echo " 9 SRC      =$(SRC) "
	@echo "10 SOURCE   =$(SOURCE) "
	@echo "11 OBJ      =$(OBJ) "
	@echo "12 OBJECT   =$(OBJECT) "
	@echo "13 CC       =$(CC)"
	@echo "14 EXE      =$(ELFEXE)"

.PHONY: clean debug

clean:
	rm -rf obj/
	rm -rf relase/
	rm -rf debug/
