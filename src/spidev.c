
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>



int spi_init( int handle )
{
  //const __u32 spi_speed  = 1953125;
  const __u32 spi_speed  = 7812500;
  const __u8 spi_wr_bits = 8;
  const __u8 spi_rd_bits = 8;
  if ( handle < 0 )
  {
    return 1;
  }

  /* mode |= SPI_LOOP; mode |= SPI_CPHA; mode |= SPI_CPOL; mode |= SPI_LSB_FIRST; mode |= SPI_CS_HIGH; mode |= SPI_3WIRE; mode |= SPI_NO_CS; mode |= SPI_READY */
  __u8 mode = 0;
  /// Set write mode
  if ( ioctl( handle, SPI_IOC_WR_MODE, &mode ) == -1 )
  {
    fprintf( stderr, "SPI set write mode error\n" );
    return 1;
  }
  /// Set read mode
  if ( ioctl( handle, SPI_IOC_RD_MODE, &mode ) == -1 )
  {
    fprintf( stderr, "SPI set read mode error\n" );
  }
  /// Set bits per write word
  if ( ioctl( handle, SPI_IOC_WR_BITS_PER_WORD, &spi_wr_bits ) == -1 )
  {
    fprintf( stderr, "SPI set bits per write word error\n" );
  }
  /// Set bits per read word
  if ( ioctl( handle, SPI_IOC_RD_BITS_PER_WORD, &spi_rd_bits ) == -1 )
  {
    fprintf( stderr, "SPI set bits per read word error\n" );
  }
  /* /// Set write speed */
  /* if ( ioctl( handle, SPI_IOC_WR_MAX_SPEED_HZ, &spi_speed ) == -1 ) */
  /* { */
  /*   fprintf( stderr, "SPI set max write speed in hz errer\n"); */
  /* } */
  /* /// Set read speed */
  /* if ( ioctl( handle, SPI_IOC_RD_MAX_SPEED_HZ, &spi_speed ) == -1 ) */
  /* { */
  /*   fprintf( stderr, "SPI set max read speed in hz errer\n"); */
  /* } */
  fprintf( stdout, "SPI mode 0x%d\n", mode );
  fprintf( stdout, "SPI write bits per word %d; SPI read bits per word %d\n", spi_wr_bits, spi_rd_bits );
  fprintf( stdout, "SPI speed: %d Hz\n", spi_speed );

  return 0;
}

int main()
{
  int spi_handle = open( "/dev/spidev1.0", O_RDWR );
  if ( spi_handle < 0 )
  {
    fprintf( stderr, "SPI open error\n" );
    return 1;
  }
  if ( spi_init( spi_handle ) )
  {
    return 2;
  }

  __u8 tx[4] = { 0x00, 0xFF, 0x80, 0xFF };
  __u8 rx[4]= { 0 };
  /**/
  int send_len = 3;
  printf( "send = %d, ", send_len );
  send_len  = write( spi_handle, tx, send_len );
  printf( "sended = %d\n", send_len );
  //usleep( 1000000 );
  int recv_len = 1; 
  printf( "recv = %d, ", recv_len );
  //recv_len = read(  spi_handle, rx, recv_len );
  printf( "recved = %d\n", recv_len );
  printf( "recv[0] value = %02x\n", rx[0] );
  printf( "recv[1] value = %02x\n", rx[1] );
  printf( "recv[2] value = %02x\n", rx[2] );
  printf( "recv[3] value = %02x\n", rx[3] );
  /**/
  /**
  struct spi_ioc_transfer spi_tx[2] = { 0 };
  spi_tx[0].tx_buf = (unsigned long)&tx[0];
  spi_tx[0].len = 3;
  spi_tx[0].speed_hz = 1953125;
  spi_tx[0].bits_per_word = 8;
  spi_tx[0].cs_change = 1;
  spi_tx[0].delay_usecs = 1;

  spi_tx[1].rx_buf = (unsigned long)&rx[0];
  spi_tx[1].len = 1;
  spi_tx[1].speed_hz = 1953125;
  spi_tx[1].bits_per_word = 8;
  spi_tx[1].cs_change = 1;
  spi_tx[1].delay_usecs = 1;
  //{
  //  .tx_buf = 0xFF,
  //  .rx_buf = 0x00,
  //  .len = 1,
  //  .delay_usecs = 0,
  //  .speed_hz = 4000000,
  //  .bits_per_word = 32
  //};
  if ( ioctl( spi_handle, SPI_IOC_MESSAGE(2), spi_tx ) < 0 )
  {
    fprintf( stderr, "SPI write message error\n" );
  }
  for ( int i = 0; i < 4; i++ )
  {
    printf( "0x%02x ", spi_tx[1].rx_buf );
  }
  printf( "\n" );
  **/
  close( spi_handle );
  return 0;
}

